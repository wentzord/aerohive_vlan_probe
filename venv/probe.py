import getopt, sys, netmiko, getpass, time, textfsm, ipaddress, subprocess, os, json, warnings
from netmiko import ConnectHandler
from os.path import dirname, join

__author__ = "Steven Bos"
__email__ = "steven@draadvrij.net"
__status__ = "Beta"

def main(argv):
    vlans = []
    AP = {}
    subnets = {}

    try:
        opts, args = getopt.getopt(argv, "s:")
        if len(opts) == 0:
            print_usage()
            sys.exit(2)
        else:
            for opt, arg in opts:
                if opt == '-s':
                    subnet = arg.strip()
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)

    username = input('AP username: ')
    password = getpass.getpass('AP Password: ')

    ip_net = ipaddress.ip_network(subnet)
    ap_ips = list(ip_net.hosts())
    ap_ips = get_pingable_aps(ap_ips) #clean up list with only pingable IP addresses

    print("Getting VLANs in use")
    for i in range(len(ap_ips)):
        device = {
            'device_type': 'generic_termserver',
            'host': str(ap_ips[i]),
            'username': username,
            'password': password,
            'port': 22
        }
        vlans = get_used_vlans(device)
        if not vlans == None and len(vlans) > 0:
            break

    for i in range(len(ap_ips)):
        device = {
            'device_type': 'generic_termserver',
            'host': str(ap_ips[i]),
            'username': username,
            'password': password,
            'port': 22
        }
        subnets = perform_dhcp_probe(device, vlans)
        if not subnets == None and len(subnets) > 0:
            AP[str(ap_ips[i])] = subnets
 
    print(AP)

def perform_dhcp_probe(_device, _vlans):
    m_subnets = {}

    current_dir = dirname(__file__)

    printProgressBar(0, len(_vlans), prefix='Testing VLANS:', suffix='Complete', length=50)

    for j in range(len(_vlans)):
        try:
            net_connect = ConnectHandler(**_device)
            time.sleep(1)
            raw_output = net_connect.send_command('show version')
            net_connect.disconnect()
            file_path = join(current_dir, "VERSION.textfsm")
            template = open(file_path)
            re_table = textfsm.TextFSM(template)
            m_results = re_table.ParseText(raw_output)
            if len(m_results) > 0: #I am an Aerohive AP
                #HiveOS till 8.4: int mgt0 dhcp-probe vlan-range 1 1
                #HiveOS from and above 8.4: int mgt0 dhcp-probe vlan-range 1-2,3,4-6
                for m_result in m_results:
                    m_major = int(m_result[0])
                    m_minor = int(m_result[1])

                if m_major >= 8 and m_minor >= 4:
                    cmd = 'int mgt0 dhcp-probe vlan-range ' + str(_vlans[j]) + '-' + str(_vlans[j])
                else:
                    cmd = 'int mgt0 dhcp-probe vlan-range ' + str(_vlans[j]) + ' ' + str(_vlans[j])
            else:
                break

            net_connect = ConnectHandler(**_device)
            time.sleep(1)
            raw_output = net_connect.send_command(cmd, expect_string='Status: complete')
            net_connect.disconnect()

            #parse show subnet info
            file_path = join(current_dir, "DHCPprobe.textfsm")
            template = open(file_path)
            re_table = textfsm.TextFSM(template)
            m_results = re_table.ParseText(raw_output)
            if len(m_results) > 0: #DHCP probe returned a subnet
                for m_result in m_results:
                    m_subnets[_vlans[j]] = m_result[0]
            else:
                m_subnets[_vlans[j]] = ''
            
            printProgressBar(j + 1, len(_vlans), prefix='Testing VLANS:', suffix='Complete', length=50)
        except Exception as ex:
            pass       
    return m_subnets

#TODO using threading to speed up shit.
def get_pingable_aps(_ap_ips):
    m_ap_ips = []
    printProgressBar(0, len(_ap_ips), prefix='Pinging devices:', suffix='Complete', length=50)

    for i in range(len(_ap_ips)):
        cmd = 'ping -c 2 ' + str(_ap_ips[i])
        try:
            output = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, encoding='UTF-8', timeout=2)
            if output.returncode == 0:
                if "Destination host unreachable" in output.stdout:
                    pass
                elif "Request timed out" in output.stdout:
                    pass
                elif "No route to host" in output.stdout:
                    pass
                else:
                    m_ap_ips.append(_ap_ips[i])
        except subprocess.TimeoutExpired:
            pass
        printProgressBar(i + 1, len(_ap_ips), prefix='Pinging devices:', suffix='Complete', length=50)
    return m_ap_ips

def get_used_vlans(_device):
    m_vlans = []

    current_dir = dirname(__file__)
    file_path = join(current_dir, "VLANS.textfsm")

    try:
        #get used VLANs from AP
        net_connect = ConnectHandler(**_device)
        time.sleep(1)
        raw_output = net_connect.send_command('show user-profile')
        net_connect.disconnect()

        #parse show user-profile output
        template = open(file_path)
        re_table = textfsm.TextFSM(template)
        m_results = re_table.ParseText(raw_output)
        for m_result in m_results:
            if not int(m_result[2]) in m_vlans:
                m_vlans.append(int(m_result[2])) #add VLAN to list
        return m_vlans
    except:
        pass

def print_usage():
    print("Usage: probe -s management subnet of APs")

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

if __name__ == "__main__":
    warnings.filterwarnings('ignore')
    main(sys.argv[1:])